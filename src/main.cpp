#include <iostream>
#include "point/TimedPoint.hpp"
#include <ios>
#include "vehicle/CapturedVehicle.hpp"
#include "data/DataParser.hpp"
#include "data/Analyzer.hpp"

static constexpr int LIMIT 				= 8;
static constexpr int UNTRUSTED_LIMIT 	= 10;

static const std::string filename 		= "../data/data001.txt";
static const std::string filename_err 	= "../data/data002.txt";


int main(int argc, char *argv[]) {

	std::vector<std::shared_ptr<CapturedVehicle>> vehicles = DataParser::parse(filename);
	std::vector<std::shared_ptr<CapturedVehicle>> fastestVehicles = Analysor::getTooFastVehicles(vehicles, LIMIT);


	std::cout << "Les véhicules suivants ont dépassés la limite : " << std::endl;
	for(const auto& vehicle: fastestVehicles){
			std::cout << vehicle << std::endl;
	}

	Analysor::removeUntrustedData(vehicles, UNTRUSTED_LIMIT);

	std::cout << "Après revue des valeurs, les véhicules suivants ont dépassés la limite : " << std::endl;
	fastestVehicles = Analysor::getTooFastVehicles(vehicles, LIMIT);
	for(const auto& vehicle: fastestVehicles){
			std::cout << vehicle << std::endl;
	}
}