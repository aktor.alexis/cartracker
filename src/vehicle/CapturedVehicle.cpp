#include <vehicle/CapturedVehicle.hpp>
std::ostream& operator<<(std::ostream& os, const std::shared_ptr<CapturedVehicle>& vehicle)
{
    os << "-> " << vehicle->m_id;
    
    return os;
}

std::shared_ptr<TimedPoint> CapturedVehicle::getPosition(int index){
    if(index < m_positions.size()){       
        return m_positions.at(index);
    }
    return nullptr;
}

void CapturedVehicle::erase(int index){
    m_positions.erase(m_positions.begin() + index);
}

bool CapturedVehicle::isTooFast(int limit){
    std::shared_ptr<TimedPoint>                     point_tmp;
    bool                                            isFirst{true};
	float                                           squared {0.0};
	float                                           average{0};
	float                                           diff {0};
	unsigned int                                    count {0};

    for(const auto& point: m_positions){
        // Impossible to compute speed with only one position. 
        if(isFirst){
            isFirst = false;
            count = 0;
            point_tmp = point;
        }
        else{
            squared = point->computeEuclidianDistance(point_tmp); // Compute euclidian distance.
            diff = point->computeDiffTime(point_tmp); // Compute timing difference.  
                                            
            average += squared / diff;
            count ++;
            point_tmp = point; // Point become the last one to compare.
        }
    }
    return ((average / count ) > limit)? true: false;
}

void CapturedVehicle::removeUntrustedData(int limit){
    std::shared_ptr<TimedPoint> point_tmp;
    float squared{0.0};
    bool isFirst{true};
    
    /* Step over positions */
    for(int i = 0; i < m_positions.size(); i++){
        if(isFirst){
            isFirst = false;
        }
        else{
            squared = m_positions.at(i)->computeEuclidianDistance(point_tmp);
            if(squared / m_positions.at(i)->computeDiffTime(point_tmp) > limit){
                erase(i); // Erase the position that is out of the limitation.
                i--;    
            }
        }
        point_tmp = m_positions.at(i); // Point become the last one to compare.
    }
    
}