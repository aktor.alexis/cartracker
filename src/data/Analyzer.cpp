#include <data/Analyzer.hpp>
#include <point/TimedPoint.hpp>
#include <math.h>
#include <iostream>
#include <vehicle/CapturedVehicle.hpp>

VehiclesCollection Analysor::getTooFastVehicles(const VehiclesCollection vehicles, int limit){
    VehiclesCollection   fastestvehicles;

    // Step over vehicles.
	for(const auto& vehicle: vehicles){
		if(vehicle->isTooFast(limit)){
            fastestvehicles.push_back(vehicle);
        }
	}
    return fastestvehicles;
}

void Analysor::removeUntrustedData(VehiclesCollection& vehicles, int limit){
    
    /* Step over vehicles */
    for(auto& vehicle: vehicles){
        vehicle->removeUntrustedData(limit);
    }
}