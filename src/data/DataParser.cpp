#include <data/DataParser.hpp>

#include <point/TimedPoint.hpp>
#include <vehicle/CapturedVehicle.hpp>

#include <fstream>
#include <errno.h>


VehiclesCollection DataParser::parse(const std::string fileName){
    /* variable declaration */
    VehiclesCollection                              vehicles;
    std::string                                     fileContent;
    std::ifstream                                   file(fileName, std::ios::binary);

    /* Getting file content */
    std::istreambuf_iterator<char>                  inputIt(file), emptyInputIt;
    std::back_insert_iterator<std::string>          stringInsert(fileContent);

    // Copy with stream iterator
    std::copy(inputIt, emptyInputIt, stringInsert);

    /* Parsing into many vehicles data */
    findVehicles(vehicles, fileContent);

    return vehicles;
}

void DataParser::findVehicles(VehiclesCollection &vehicles, const std::string& fileContent){
    /* variable declaration */
    size_t  start;
	size_t  end{0};

    /* Stepping over vehicules substring */
	while ((start = fileContent.find_first_not_of(VEHICLE_DELIMITER, end)) != std::string::npos){
		end = fileContent.find(VEHICLE_DELIMITER, start);
        std::shared_ptr<CapturedVehicle> vehicle = getVehicle(fileContent.substr(start, end - start));
        if(vehicle != nullptr){
            vehicles.push_back(vehicle);
        }
	}
}

std::shared_ptr<CapturedVehicle> DataParser::getVehicle(const std::string& str){
    /* Variable declaration */
    size_t                                      start;
	size_t                                      end{0};
    int                                         id;
    PointsCollection                            points;

    /* Getting vehicle id */
	if ((start = str.find_first_not_of(ID_DELIMITER, end)) != std::string::npos)
	{
		end = str.find(ID_DELIMITER, start);

        try{
            id = std::stoi(str.substr(start, end - start));
        }
        catch(const std::invalid_argument& err){
            std::cerr << "Erreur: id non valide: valeur inconnue." << err.what() << std::endl;
            return nullptr;
        }
        catch(const std::out_of_range& err){
            std::cerr << "Erreur: id non valide: valeur trop grande." << std::endl;
        }
    }
    else{
        std::cout << "Erreur: identifiant du véhicule illisible: " << str.substr(start, end - start) << std::endl;
        return nullptr;
    }
    /* Getting points substring */
    if ((start = str.find_first_not_of(ID_DELIMITER, end)) != std::string::npos)
	{
		end = str.find(ID_DELIMITER, start);
        /* Getting all points */
		points = getPoints(str.substr(start, end - start));
    }
    else{
        std::cout << "Erreur: pas de données pour le véhicule: " << id << std::endl; 
    }

	
    return std::make_shared<CapturedVehicle>(id, points);
}

PointsCollection DataParser::getPoints(const std::string str){
    /* Variable declaration */
    size_t                                      start;
	size_t                                      end{0};

    size_t                                      start_1;
	size_t                                      end_1{0};

    int                                         id{0};

    PointsCollection                            outDataPoints;
    int                                         coordinates[DIMENSION];
    bool                                        isValid{true};

    /* Stepping over points */
	while ((start = str.find_first_not_of(POINT_DELIMITER, end)) != std::string::npos){
		end = str.find(POINT_DELIMITER, start);
        std::string point = str.substr(start, end - start);
        /* Checking point is not an empty string */
        if(point != ""){
            /* Stepping over each coordinate */
            while ((start_1 = point.find_first_not_of(COORDINATE_DELIMITER, end_1)) != std::string::npos){
                end_1 = point.find(COORDINATE_DELIMITER, start_1);
                /* Take only the three first values : other are errors */
                if(id < DIMENSION){
                    try{
                        coordinates[id] = std::stoi(point.substr(start_1, end_1 - start_1));
                    }
                    catch(const std::invalid_argument& err){
                         isValid = false;
                        std::cerr << "Erreur: position non valide: valeur inconnue." << err.what() << std::endl;
                    }
                    catch(const std::out_of_range& err){
                        isValid = false;
                        std::cerr << "Erreur: position non valide: valeur trop grande" << err.what() << std::endl;
                    }
                }
                id ++;
            }

            if(isValid == true){
                if(id == DIMENSION){
                    std::shared_ptr<TimedPoint> pointRet = std::make_shared<TimedPoint>(coordinates[TimedPoint::X], coordinates[TimedPoint::Y], coordinates[TimedPoint::T]);
                    outDataPoints.push_back(pointRet);
                    isValid = true;
                }
                else if(id > DIMENSION){
                    std::cout << "Erreur: il y a trop de coordonnées: " << point << std::endl;
                }
                else{
                    std::cout << "Erreur: il manque une des coordonnées: " << point << std::endl;
                }
            }

            id = 0;
            end_1 = 0;
        }
        else{
            std::cout << "Erreur: point vide" << std::endl;
        }
	}
    return outDataPoints;
}
