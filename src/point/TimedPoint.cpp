#include <point/TimedPoint.hpp>
#include <memory>
#include <math.h>

int TimedPoint::computeEuclidianDistance(std::shared_ptr<TimedPoint> point){
      return sqrt(pow(m_x - point->m_x, 2) + pow(m_x - point->m_y, 2));
}

unsigned int TimedPoint::computeDiffTime(std::shared_ptr<TimedPoint> point){
      return (point->m_time > m_time) ? (point->m_time - m_time) : (m_time - point->m_time);
}

std::ostream& operator<<(std::ostream& os, const std::shared_ptr<TimedPoint>& point)
{
      os << point->m_x << '/' << point->m_y << '/' << point->m_time;
      return os;
}