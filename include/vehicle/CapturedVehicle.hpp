#ifndef __CAPTUREDVEHICLE_H__
#define __CAPTUREDVEHICLE_H__
#include <memory>
#include <vector>
#include <point/TimedPoint.hpp>


/**
 * @class CapturedVehicle
 * @brief A captured vehicle is a vehicle with a set of point of positions.
 */
class CapturedVehicle{
  public:
    /**
     * @brief getPositions.
     * @return m_positions Get all position points.
     */
    std::vector<std::shared_ptr<TimedPoint>> getPositions() {return m_positions;}

    /**
     * @brief constructor.
     * @param id Id of the vehicle.
     * @param t_positions List of positions.
     */
    CapturedVehicle(int t_id, std::vector<std::shared_ptr<TimedPoint>> t_positions): m_positions(t_positions), m_id(t_id){}
    
    /**
     * @brief Destructor.
     */
    ~CapturedVehicle(){};
    
    /**
     * @brief Get the position with the specified id.
     * @param index Id of the position index.
     * @return A point.
     */
    std::shared_ptr<TimedPoint> getPosition(int index);

    /**
     * @brief Erase the position with the specified id.
     * @param index Id of the position index.
     */
    void erase(const int index);

    bool isTooFast(int limit);
    void removeUntrustedData(int limit);

    /**
     * @brief overload the << operator to allow the std::cout to print a Captured Vehicle.
     * @param os ostream operator.
     * @param point The vehicle to print.
     */
    friend std::ostream& operator<<(std::ostream& os, const std::shared_ptr<CapturedVehicle>& vehicle);

  private:
    std::vector<std::shared_ptr<TimedPoint>> m_positions;
    int m_id;
};
#endif // __CAPTUREDVEHICLE_H__