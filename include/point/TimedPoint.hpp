#ifndef __TIMEDPOINT_H__
#define __TIMEDPOINT_H__

#include <iostream>
#include <memory>

constexpr int DIMENSION = 3;
  
/** 
 * @class Point
 * @brief This class contains a timed coordinates value.
 * 
 */
class TimedPoint{
  public:
    /**
    * @brief constructor.
    */
    TimedPoint(int t_x, int t_y, unsigned int t_time): m_x(t_x), m_y(t_y), m_time(t_time) { }

    /**
     * @brief Destructor.
     */
    virtual ~TimedPoint(){};
    
    int computeEuclidianDistance(std::shared_ptr<TimedPoint> point);

    unsigned int computeDiffTime(std::shared_ptr<TimedPoint> point);

    /**
     * @brief overload the << operator to allow the std::cout to print a TimedPoint.
     * @param os ostream operator.
     * @param point The point to print.
     */
    friend std::ostream& operator<<(std::ostream& os, const std::shared_ptr<TimedPoint>& point);
    
    /**
     * @enum Enumerate point attributes.
     * @brief It should be used to help for some algorithm.
     */
    enum Coordinate{
      X = 0,
      Y = 1,
      T = 2
    };
    
  private:
    unsigned int  m_time;
    int           m_x;
    int           m_y;
};


#endif // __TIMEDPOINT_H__