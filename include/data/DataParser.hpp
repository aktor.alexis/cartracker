#ifndef __DATAPARSER_H__
#define __DATAPARSER_H__
#include <iostream>
#include <vector>
#include <cstring>
#include <memory>


class TimedPoint;
class CapturedVehicle;

using VehiclesCollection = std::vector<std::shared_ptr<CapturedVehicle>>;
using PointsCollection   = std::vector<std::shared_ptr<TimedPoint>>;

static constexpr char VEHICLE_DELIMITER     = ';';
static constexpr char ID_DELIMITER          = '|';
static constexpr char COORDINATE_DELIMITER  = '*';
static constexpr char POINT_DELIMITER       = '/';

/**
 * @class DataParser
 * @brief Parse from a file vehicles points.
 */
class DataParser{
  public:
    /*
    * @brief Parse a file that represents a list of vehicles with timed points (X*Y*Z).
    * @param FileName The name of the file to parse.
    * @return A list of captured vehicles.
    */
    static VehiclesCollection parse(const std::string fileName);  
    

    private:
      static std::shared_ptr<CapturedVehicle> getVehicle(const std::string& str);
      static void findVehicles(VehiclesCollection &vehicles, const std::string& fileContent);
      static PointsCollection getPoints(const std::string str);
};
#endif // __DATAPARSER_H__