#ifndef __ANALYZER_H__
#define __ANALYZER_H__

#include <vector>
#include <memory>

class CapturedVehicle;

using VehiclesCollection = std::vector<std::shared_ptr<CapturedVehicle>>;

/**
 * @class Analysor
 * @brief Tools to analyse a list of vehicles.
 */
class Analysor{
    public: 
        /**
         * @brief Getting all vehicles that are out of the limitation.
         * @param vehicles The list of vehicles to look at.
         * @param limit The limitation.
         * @return All vehicles that are out of the limitation.
         */
        static VehiclesCollection getTooFastVehicles(VehiclesCollection vehicles, int limit);

        /**
         * @brief removing all vehicles that are out of the limitation.
         * @param vehicles The list of vehicles to look at.
         * @param limit The limitation.
         */
        static void removeUntrustedData(VehiclesCollection& vehicles, int limit);
};

#endif // __ANALYZER_H__